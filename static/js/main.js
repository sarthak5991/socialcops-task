//Kinks left to Iron out
//	1. the "catchLink" function only works if link not in last para.
//	2. the toolTip can glitch if there are multiple clicks, otherwise its fine.
//	3. the fps drops a little if there are multiple consecutive toolTips. 
//	4. the Randomize function doesnt work with full accuracy.


var main = function() {
	rangy.init();
	
	pressEnter();								//funtion for Task 1
	dragPara();									//function for task 
	showToolTip();								//function for task 2
	
	$("#button-to-linkify").on("click", function() {
		console.log("linkify button clicked");
		catchLink();							//function for task 3
	});

	//Advanced Task
	$("#button-to-randomize").on("click", function() {
		console.log("randomize button clicked");
		randomizeWords();						//function for Advanced task 
	});

};

$(document).ready(main);

var pressEnter = function() {
	console.log("entered pressEnter function");

	var ENTER = 13;
	// var HTMLToENTER = '</p></div> <div><p> <div contenteditable="false" class="move-icon-div"> <i class="move icon"></i> </div>';

	$(document).on("keypress", "#text-editor-area", function(event) {
		console.log("key pressed");
		if (event.keyCode === ENTER) {
			console.log("enter pressed");
			event.preventDefault();
			var paraContent = $("p.new-para-content").text();
			$("<div class='para-div'><div class='move-icon-div'><i class='move icon'></i></div><p>" + paraContent + "</p><br></div>").insertBefore("p.new-para-content");
			$("p.new-para-content").text(" ");
		}
	});

	$("#text-editor-area").on("click", "p", function(event) {
		$("p.new-para-content").removeClass("new-para-content").attr("contenteditable", false);
		$(this).addClass("new-para-content").attr("contenteditable",true).focus();
	});
};


var dragPara = function() {
	console.log("entered dragPara function");
	$('#text-editor-area').livequery(".para-div", function() {
		$(".para-div").hoverIntent({
			sensitivity: 1,
			interval: 40,
			timeout: 150,
			over:function() {
				console.log("hoverIntent - over");
				$(this).children(".move-icon-div").fadeIn(100);

				$(this).children(".move-icon-div").mousedown(function() {
					console.log('on mouse down');
					$(this).children("p").attr('contenteditable','false');
					dragula([document.getElementById('text-editor-area')]);
					console.log('dragula implemented');
				});
				$('body').mouseup( function() {
					console.log('on mouse up');
					$(this).children("p").attr('contenteditable','true');
				});
				
			},
			out:function() {
				console.log("hoverIntent - out");
				$(this).children(".move-icon-div").fadeOut(100);
			}
		}), function() {
			$(this).unbind('mouseover').unbind('mouseout');
		}
	});
};

var showToolTip = function() {
	console.log("entered showToolTip function");
	$("#text-editor-area").livequery("p", function() {
		console.log("entered livequery");
		$(this).dblclick( function() {
			console.log("double clicked");
			var selection = rangy.getSelection();
			var textInSelection = selection.toString();
			var count = (textInSelection.match(/ /g) || []).length;
			console.log("no. of spaces - " + count);

			if (count <= 1 && textInSelection.length > 1) {
				console.log("entered first if in dblclick")
				var toolTip = '<button class="ui button" id="button-b">B</button><button class="ui button" id="button-u">U</button><button class="ui red button" id=button-r>R</button>';
					
				$(this).powerTip({
					placement: 'w',
					manual: true
				});	
				$(this).data('powertip', toolTip);
				$(this).powerTip('show');


				$(this).on({
					'blur': function() {
						console.log("blurred");
						$(this).powerTip('hide', true);
						$(this).powerTip("destroy");
					}
				});

				$("#powerTip").children("#button-b").click(function() {
					console.log("button-b pressed");
					applierBold = rangy.createClassApplier("make-bold");
					applierBold.toggleSelection();
				});
				$("#powerTip").children("#button-u").click(function() {
					console.log("button-b pressed");
					applierUnderline = rangy.createClassApplier("make-underline");
					applierUnderline.toggleSelection();
				});
				$("#powerTip").children("#button-r").click(function() {
					console.log("button-b pressed");
					applierRed = rangy.createClassApplier("make-red");
					applierRed.toggleSelection();
				});


			}
		});
	});
};

var catchLink = function() {
	$("#catched-links-div").html(" ");						//to not repeat previous links
	console.log("entered function catchLink");
	var totalEditorText = $("#text-editor-area").html();

	var openingACount = (totalEditorText.match(new RegExp("<a>", "g")) || []).length;
	console.log("openingACount = " + openingACount);
	var closingACount = (totalEditorText.match(new RegExp("</a>","g")) || []).length;
	console.log("closingACount = " + closingACount);
	var aCount = Math.min(openingACount, closingACount);
	console.log("aCount = " + aCount);
	
	var lastPosition = 0;
	var positionOfStartTag = -1;
	var positionOfEndTag = -1;
	var oddEvenDetermine = 0;

	for (var i = 0; i < aCount; i++) {
		console.log("catchLink - entered loop " + i);
		positionOfStartTag = totalEditorText.indexOf("<a>", lastPosition);
		console.log("positionOfStartTag = " + positionOfStartTag);
		positionOfEndTag = totalEditorText.indexOf("</a>", lastPosition);
		console.log("positionOfEndTag = " + positionOfEndTag);
		var linkLength = positionOfEndTag - positionOfStartTag - 3;
		console.log("linkLength = " + linkLength);

		var linkHREF = totalEditorText.slice(positionOfStartTag + 3, positionOfEndTag);
		console.log("linkHREF = " + linkHREF);

		lastPosition = positionOfEndTag + 2;
		if (oddEvenDetermine % 2 == 1) {
			console.log("label added as odd");
			$("#catched-links-div").append("<a class='ui red tag label' href='" + linkHREF + "'>" + linkHREF + "</a>");
			oddEvenDetermine++;
		} else {
			console.log("label added as even");
			$("#catched-links-div").append("<a class='ui blue tag label' href='" + linkHREF + "'>" + linkHREF + "</a>");
			oddEvenDetermine++;
		}
	}

};

var fourLeterWordCounta;
var fourLeterWordCountb;
var fourLeterWordCountTot;
var paraTextArray = [];

var randomizeWords = function() {
	var i = 1;
	var length = $("#text-editor-area").children().length;
	console.log("length - " + length);
	formParaArray();
	while (i<=length) {
		console.log("randomizeWords while entered");
		
		fourLeterWordCounta = (paraTextArray[i-1].match(/\s\D\D\D\D\s/g) || []).length;
		console.log("Word Counta = " + fourLeterWordCounta);
		fourLeterWordCountb = (paraTextArray[i-1].match(/\s\D\D\D\D$/g) || []).length;
		console.log("Word Countb = " + fourLeterWordCountb);
		fourLeterWordCountc = (paraTextArray[i-1].match(/^\D\D\D\D\s/g) || []).length;
		console.log("Word Countc = " + fourLeterWordCountc);
		fourLeterWordCountTot = fourLeterWordCounta + fourLeterWordCountb + fourLeterWordCountc;
		console.log("Word Count Total = " + fourLeterWordCountTot);

		paraTextArray[i-1] = paraTextArray[i-1].replace(/\s\D\D\D\D\s/g, " qpzm ");
		paraTextArray[i-1] = paraTextArray[i-1].replace(/\s\D\D\D\D$/g, " qpzm ");
		paraTextArray[i-1] = paraTextArray[i-1].replace(/^\D\D\D\D\s/g, " qpzm ");
		console.log("qwe "+paraTextArray[i-1]);

		if (fourLeterWordCountTot>0) {
			console.log("entered first if");
				console.log("entered second if");
				var j;
				for(j=0 ; j<fourLeterWordCountTot ; j++) {
					console.log("randomizeWords for A entered " + j);
					getRandomWord(i-1);
			}
		}

		i++;
	}

	setTimeout(function() {
		for(var k=0;k<=length;k++) {
			console.log("in final loop" + k);
			$("#text-editor-area").children("div:nth-child("+(k+1)+")").children("p").html(paraTextArray[k]);
		}
		$("#text-editor-area").children("p").html(paraTextArray[length-1]);
	},2000);


};

var getRandomWord = function(i) {				//function to get random words by an asynch AJAX request in a loop
	console.log("firing ajax request");
	$.ajax({
        type: "GET",
        url: "http://randomword.setgetgo.com/get.php",
        async: false,
        dataType: "jsonp",
        success: function(data) {
           	console.log("GET request success for " + data.Word);
           	console.log("jgjgjg" +paraTextArray[i]);
           	paraTextArray[i] = paraTextArray[i].replace("qpzm", " "+data.Word+" ");
           	console.log("asdosdfml");
        },
        error: function() {
        	console.log("GET request error");
        	return "err";
        }
    });
};

var formParaArray = function () {							//form an array of children p elements 
	console.log("formParaArray function entered");
	var totalChildren = $("#text-editor-area").children().length;
	var i=1;
	for(i=1;i<=totalChildren;i++) {
		paraTextArray[i-1] = $("#text-editor-area").children("div:nth-child("+i+")").children("p").html();
	}
	paraTextArray[totalChildren-1] = $("#text-editor-area").children("p").html();
	console.log(paraTextArray)
	console.log("formParaArray finished");
}