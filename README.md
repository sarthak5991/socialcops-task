I have complete all 3 tasks along with Advanced Task 

Simply run main.html for the template to load   
main.js is in /static/js

I have used the following Jquery/javascript libraries -   
 1. Rangy by Tim Down - [https://github.com/timdown/rangy](Link URL)   
 2. livequery by Brandon Aaron - [https://github.com/brandonaaron/livequery](Link URL)  
 3. dragula by Nicolas Bevacqua - [https://github.com/bevacqua/dragula](Link URL)  
 4. powertip by Steven Benner - [http://stevenbenner.github.io/jquery-powertip/](Link URL)  
 5. hoverIntent by Brian Cherne - [https://github.com/briancherne/jquery-hoverIntent](Link URL)  

The site also uses Semantic-UI Framework   
   
Here is the Task Details - https://quip.com/2T5EAf6Ab7CO
   
**Site Hosted Here -** http://sarthak-editit.bitballoon.com/